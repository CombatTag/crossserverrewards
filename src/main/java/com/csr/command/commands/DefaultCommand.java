package com.csr.command.commands;

import com.csr.CrossServerRewards;
import com.csr.command.ICommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by derek on 8/28/2015.
 */
public class DefaultCommand implements ICommand {

    @Override
    public String getName() {
        return "";
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public String getUsageString(String label, CommandSender sender) {
        return label;
    }

    @Override
    public String getDescription() {
        return "Opens the cross server rewards main GUI.";
    }

    @Override
    public boolean canBeConsole() {
        return false;
    }

    @Override
    public boolean canBeCommandBlock() {
        return false;
    }

    @Override
    public boolean onCommand(CommandSender sender, String label, String... args) {
        if (args.length != 0) {
            return false;
        }

        CrossServerRewards crossServerRewards = CrossServerRewards.getInstance();
        Player player = (Player) sender;

        if (crossServerRewards.getConfigOptions().isServerRewardClaim()) {
            crossServerRewards.getGuiManager().openRewardGui(player);
        } else {
            crossServerRewards.getGuiManager().openMainGui(player);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String label, String... args) {
        return null;
    }
}
