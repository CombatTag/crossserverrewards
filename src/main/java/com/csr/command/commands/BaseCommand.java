package com.csr.command.commands;

import com.csr.command.ICommand;
import com.csr.util.Base64Util;
import com.google.common.base.Splitter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Iterator;
import java.util.List;

/**
 * Created by derek on 8/28/2015.
 */
public class BaseCommand implements ICommand {

    @Override
    public String getName() {
        return "base";
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public String getUsageString(String label, CommandSender sender) {
        return label;
    }

    @Override
    public String getDescription() {
        return "Coverts the item in hand to base 64.";
    }

    @Override
    public boolean canBeConsole() {
        return false;
    }

    @Override
    public boolean canBeCommandBlock() {
        return false;
    }

    @Override
    public boolean onCommand(CommandSender sender, String label, String... args) {
        if (args.length != 0) {
            return false;
        }
        Player player = (Player) sender;

        ItemStack[] itemStacks = {player.getItemInHand()};
        Iterator<String> iterator = Splitter.fixedLength(100).split(Base64Util.objectArrayToBase64(itemStacks)).iterator();
        int i = 1;
        while (iterator.hasNext()) {
            TextComponent textComponent = new TextComponent(ChatColor.GRAY + "Base 64 " + ChatColor.GOLD + i);
            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, iterator.next()));
            player.spigot().sendMessage(textComponent);
            i++;
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String label, String... args) {
        return null;
    }
}
