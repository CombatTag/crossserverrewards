package com.csr.command;

import com.csr.CrossServerRewards;
import com.csr.command.commands.BaseCommand;
import com.csr.command.commands.DefaultCommand;
import org.bukkit.command.PluginCommand;

/**
 * Created by derek on 8/28/2015.
 */
public class CommandManager {

    private final CommandDispatcher commandDispatcher;

    public CommandManager() {
        PluginCommand pluginCommand = CrossServerRewards.getInstance().getCommand("reward");
        commandDispatcher = new CommandDispatcher("reward", "Cross server rewards main command.");

        commandDispatcher.setDefault(new DefaultCommand());
        commandDispatcher.registerCommand(new BaseCommand());

        pluginCommand.setExecutor(commandDispatcher);
        pluginCommand.setTabCompleter(commandDispatcher);
    }
}
