package com.csr.gui;

import com.csr.CrossServerRewards;
import com.csr.gui.items.GuiItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Created by derek on 9/6/2015.
 */
public class Gui implements Listener {

    private final Gui parent;
    private final List<HumanEntity> humanEntities = new ArrayList<>();
    private Map<Integer, GuiItem> items = new HashMap<>();
    private final String name;

    public Gui(Gui parent, String name, Map<Integer, GuiItem> items) {
        this.parent = parent;
        this.name = name;
        this.items = items;

        Bukkit.getPluginManager().registerEvents(this, CrossServerRewards.getInstance());
    }

    public void displayGui(HumanEntity humanEntity) {
        humanEntities.add(humanEntity);

        Inventory inventory = Bukkit.createInventory(humanEntity, 54, name);

        for (Map.Entry<Integer, GuiItem> entry : items.entrySet()) {
            inventory.setItem(entry.getKey(), entry.getValue().getItemStack());
        }

        if (parent != null) {
            ItemStack itemStack = new ItemStack(Material.BARRIER);
            ItemMeta itemMeta = itemStack.getItemMeta();

            itemMeta.setDisplayName("\2474\247lGo Back");

            itemStack.setItemMeta(itemMeta);

            inventory.setItem(45, itemStack);
        }

        humanEntity.openInventory(inventory);
    }

    @EventHandler
    public void onInventoryMoveItem(InventoryMoveItemEvent event) {
        if (event.getDestination().getName().equals(name)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        Inventory topInventory = event.getView().getTopInventory();

        if (topInventory != null) {
            if (topInventory.getName().equals(name)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        Inventory topInventory = event.getView().getTopInventory();

        if (topInventory != null) {
            if (event.getClickedInventory() != null) {
                if (topInventory.getName().equals(name)) {
                    GuiItem guiItem = null;

                    if (topInventory == event.getClickedInventory()) {
                        guiItem = items.get(event.getSlot());
                    }

                    if (event.getClickedInventory() == topInventory) {
                        if (humanEntities.contains(event.getWhoClicked())) {
                            if (parent != null && event.getSlot() == 45) {
                                event.setCancelled(true);
                                new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        event.getWhoClicked().closeInventory();
                                        parent.displayGui(event.getWhoClicked());
                                    }
                                }.runTask(CrossServerRewards.getInstance());
                            } else {
                                if (guiItem != null) {
                                    final GuiItem runnableItem = guiItem;
                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            runnableItem.onClick(event.getWhoClicked());
                                        }
                                    }.runTask(CrossServerRewards.getInstance());
                                }
                            }
                        }
                    }

                    switch (event.getAction()) {
                        case DROP_ALL_CURSOR:
                            if (topInventory != event.getClickedInventory() || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case DROP_ALL_SLOT:
                            if (topInventory != event.getClickedInventory() || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case DROP_ONE_CURSOR:
                            if (topInventory != event.getClickedInventory() || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case DROP_ONE_SLOT:
                            if (topInventory != event.getClickedInventory() || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case HOTBAR_MOVE_AND_READD:
                            if (event.getCurrentItem().getType() != Material.AIR && (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case HOTBAR_SWAP:
                            if (event.getCurrentItem().getType() != Material.AIR && (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case MOVE_TO_OTHER_INVENTORY:
                            if (event.getClickedInventory() == topInventory && (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case NOTHING:
                            if (topInventory != event.getClickedInventory() || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case PICKUP_ALL:
                            if (topInventory != event.getClickedInventory() || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case PICKUP_HALF:
                            if (topInventory != event.getClickedInventory() || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case PICKUP_ONE:
                            if (topInventory != event.getClickedInventory() || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case PICKUP_SOME:
                            if (topInventory != event.getClickedInventory() || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case PLACE_ALL:
                            if (event.getClickedInventory() != topInventory || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case PLACE_ONE:
                            if (event.getClickedInventory() != topInventory || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case PLACE_SOME:
                            if (event.getClickedInventory() != topInventory || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        case SWAP_WITH_CURSOR:
                            if (event.getClickedInventory() != topInventory || (guiItem != null && guiItem.isMovable())) {
                                return;
                            } else {
                                event.setCancelled(true);
                            }
                        default:
                            event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    private void onInventoryClose(InventoryCloseEvent event) {
        Inventory topInventory = event.getView().getTopInventory();

        if (topInventory != null) {
            if (topInventory.getName().equals(name)) {
                if (humanEntities.contains(event.getPlayer())) {
                    humanEntities.remove(event.getPlayer());
                }
            }
        }
    }

    public Map<Integer, GuiItem> getItems() {
        return items;
    }

    public void disableGui() {
        for (HumanEntity humanEntity : humanEntities) {
            humanEntity.closeInventory();
        }
        HandlerList.unregisterAll(this);
    }
}
