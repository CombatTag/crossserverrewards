package com.csr.gui;

import com.csr.CrossServerRewards;
import com.csr.gui.items.GuiItem;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by derek on 9/12/2015.
 */
public class RewardGui extends Gui {

    private final String name;
    private final String serverName;

    public RewardGui(Gui parent, String name, Map<Integer, GuiItem> items, String serverName) {
        super(parent, name, items);

        this.name = name;
        this.serverName = serverName;
    }

    @EventHandler
    public void onInventoryMoveItem(InventoryMoveItemEvent event) {
        super.onInventoryMoveItem(event);
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        super.onInventoryDrag(event);
    }

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        super.onInventoryClick(event);
    }

    @EventHandler
    public void onCloseInventory(InventoryCloseEvent event) {
        Inventory topInventory = event.getView().getTopInventory();
        if (topInventory != null) {
            if (topInventory.getName().equals(name)) {
                List<ItemStack> items = new ArrayList<>();
                for (ItemStack itemStack : event.getInventory().getContents()) {
                    if (itemStack != null) {
                        items.add(itemStack);
                    }
                }

                CrossServerRewards.getInstance().getIDatabase().saveRewards(event.getPlayer().getUniqueId(), serverName, items.toArray(new ItemStack[items.size()]));
                HandlerList.unregisterAll(this);
            }
        }
    }
}
