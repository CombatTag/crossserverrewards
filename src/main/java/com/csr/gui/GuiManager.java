package com.csr.gui;

import com.csr.CrossServerRewards;
import com.csr.database.Callback;
import org.bukkit.entity.HumanEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by derek on 9/7/2015.
 */
public class GuiManager {

    private Gui mainGui;
    private Map<String, Gui> serverGuis = new HashMap<>();

    public GuiManager() {
        loadGuis();
    }

    public void loadGuis() {
        CrossServerRewards.getInstance().getIDatabase().getMainGui(new Callback<Gui>() {
            @Override
            public void onSuccess(Gui value) {
                mainGui = value;
            }

            @Override
            public void onFailure() {}
        });

        CrossServerRewards.getInstance().getIDatabase().getServerGuis(new Callback<Map<String, Gui>>() {
            @Override
            public void onSuccess(Map<String, Gui> value) {
                serverGuis = value;
            }

            @Override
            public void onFailure() {}
        });
    }

    public void openRewardGui(final HumanEntity humanEntity) {
        CrossServerRewards.getInstance().getIDatabase().getRewardGui(humanEntity.getUniqueId(), CrossServerRewards.getInstance().getConfigOptions().getServerName(), new Callback<Gui>() {
            @Override
            public void onSuccess(Gui value) {
                humanEntity.closeInventory();
                value.displayGui(humanEntity);
            }

            @Override
            public void onFailure() {
            }
        });
    }

    public void openMainGui(HumanEntity humanEntity) {
        humanEntity.closeInventory();
        mainGui.displayGui(humanEntity);
    }

    public void openServerGui(HumanEntity humanEntity, String server) {
        if (serverGuis.containsKey(server)) {
            humanEntity.closeInventory();
            serverGuis.get(server).displayGui(humanEntity);
        }
    }

    public Gui getMainGui() {
        return mainGui;
    }
}
