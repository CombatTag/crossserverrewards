package com.csr.gui.items;

import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.ItemStack;

/**
 * Created by derek on 9/6/2015.
 */
public class GuiItem {

    private final ItemStack itemstack;

    public GuiItem(ItemStack itemStack) {
        this.itemstack = itemStack;
    }

    public ItemStack getItemStack() {
        return itemstack;
    }

    public boolean isMovable() {
        return true;
    }

    public void onClick(HumanEntity humanEntity) {}
}
