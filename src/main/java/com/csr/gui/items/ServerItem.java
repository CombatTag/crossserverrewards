package com.csr.gui.items;

import com.csr.CrossServerRewards;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.ItemStack;

/**
 * Created by derek on 9/6/2015.
 */
public class ServerItem extends GuiItem {

    private final String server;

    public ServerItem(ItemStack itemStack, String server) {
        super(itemStack);

        this.server = server;
    }

    @Override
    public boolean isMovable() {
        return false;
    }

    @Override
    public void onClick(HumanEntity humanEntity) {
        super.onClick(humanEntity);

        CrossServerRewards.getInstance().getGuiManager().openServerGui(humanEntity, server);
    }
}
