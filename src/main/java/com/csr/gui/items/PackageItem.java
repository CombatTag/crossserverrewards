package com.csr.gui.items;

import com.csr.CrossServerRewards;
import com.csr.config.objects.PackageItemStack;
import com.csr.database.Callback;
import com.csr.gui.Gui;
import com.csr.util.ChatUtil;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * Created by derek on 9/6/2015.
 */
public class PackageItem extends GuiItem {

    private final String serverName;
    private final int price;
    private final PackageItemStack[] packageItemStacks;
    private final List<HumanEntity> humanEntities = new ArrayList<>();

    public PackageItem(ItemStack itemStack, String serverName, int price, PackageItemStack... packageItemStacks) {
        super(itemStack);

        this.serverName = serverName;
        this.price = price;
        this.packageItemStacks = packageItemStacks;
    }

    @Override
    public boolean isMovable() {
        return false;
    }

    @Override
    public void onClick(final HumanEntity humanEntity) {
        super.onClick(humanEntity);

        if (!humanEntities.contains(humanEntity)) {
            humanEntities.add(humanEntity);
            Bukkit.getScheduler().runTaskLater(CrossServerRewards.getInstance(), new Runnable() {
                @Override
                public void run() {
                    humanEntities.remove(humanEntity);
                }
            }, 20);

            CrossServerRewards.getInstance().getIDatabase().getRewardGui(humanEntity.getUniqueId(), serverName, new Callback<Gui>() {
                @Override
                public void onSuccess(Gui value) {
                    if (CrossServerRewards.getInstance().getEconomy().getBalance(Bukkit.getOfflinePlayer(humanEntity.getUniqueId())) >= price) {
                        List<ItemStack> items = new ArrayList<>();
                        ComponentBuilder componentBuilder = new ComponentBuilder("");

                        for (PackageItemStack packageItemStack : packageItemStacks) {
                            double random = Math.random() * 100;

                            if (random < packageItemStack.getChance()) {
                                items.add(packageItemStack.getItemStack());
                            }
                        }

                        CrossServerRewards.getInstance().getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(humanEntity.getUniqueId()), price);
                        if (items.isEmpty()) {
                            componentBuilder
                                    .append("\u00BB ").color(ChatColor.DARK_RED)
                                    .append("You lost ").color(ChatColor.RED)
                                    .append(String.valueOf(price)).color(ChatColor.GRAY)
                                    .append(" and did not get any items as the odds were not in your favour. Try again?").color(ChatColor.RED);

                            Bukkit.getPlayer(humanEntity.getUniqueId()).spigot().sendMessage(componentBuilder.create());
                        } else {
                            Location location = humanEntity.getEyeLocation().toVector().add(humanEntity.getEyeLocation().getDirection().multiply(.5)).toLocation(humanEntity.getWorld());
                            Iterator<ItemStack> itemsIterator = items.iterator();

                            componentBuilder
                                    .append("\u00BB ").color(ChatColor.DARK_GREEN)
                                    .append("You have successfully paid ").color(ChatColor.GREEN)
                                    .append(String.valueOf(price)).color(ChatColor.GRAY)
                                    .append(", which got you these items:").color(ChatColor.GREEN);

                            while (itemsIterator.hasNext()) {
                                ItemStack itemStack = itemsIterator.next();

                                componentBuilder
                                        .append("\n")
                                        .event(new HoverEvent(HoverEvent.Action.SHOW_ITEM, null))
                                        .append("  " + String.valueOf(itemStack.getAmount() + "x ")).color(ChatColor.GRAY)
                                        .append(itemStack.getType().name()).color(ChatColor.GOLD)
                                        .event(new HoverEvent(HoverEvent.Action.SHOW_ITEM,
                                                new BaseComponent[]{
                                                        new TextComponent(ChatUtil.convertItemStackToJson(itemStack))
                                                }));
                            }

                            componentBuilder
                                    .append("\n")
                                    .event(new HoverEvent(HoverEvent.Action.SHOW_ITEM, null))
                                    .append("\u00BB ").color(ChatColor.DARK_GREEN)
                                    .append("Claim your rewards on ").color(ChatColor.GREEN)
                                    .append(serverName).color(ChatColor.GRAY)
                                    .append(" by saying ").color(ChatColor.GREEN)
                                    .append("/reward").color(ChatColor.GRAY);

                            for (Map.Entry<Integer, GuiItem> entry : value.getItems().entrySet()) {
                                GuiItem guiItem = entry.getValue();

                                if (!(guiItem instanceof PackageItem || guiItem instanceof ServerItem)) {
                                    items.add(guiItem.getItemStack());
                                }
                            }

                            int i = 0;
                            for (ItemStack itemStack : items) {
                                if (itemStack != null) {
                                    i++;
                                }
                            }

                            if (i > 54) {
                                humanEntity.sendMessage(ChatColor.RED + "You don't have enough space in your reward inventory.");
                            } else {
                                CrossServerRewards.getInstance().getIDatabase().saveRewards(humanEntity.getUniqueId(), serverName, items.toArray(new ItemStack[items.size()]));
                                humanEntity.getWorld().spigot().playEffect(location, Effect.HAPPY_VILLAGER, 0, 0, (float) 0.25, (float) 0.25, (float) 0.25, (float) 0.01, 50, 50);
                                Bukkit.getPlayer(humanEntity.getUniqueId()).spigot().sendMessage(componentBuilder.create());
                            }
                        }
                    } else {
                        humanEntity.sendMessage(ChatColor.RED + "This cost " + ChatColor.GRAY + price + ChatColor.RED + " and you only have " + ChatColor.GRAY + CrossServerRewards.getInstance().getEconomy().getBalance(Bukkit.getOfflinePlayer(humanEntity.getUniqueId())) + ChatColor.RED + ".");
                    }
                }

                @Override
                public void onFailure() {
                }
            });
        }
    }
}
