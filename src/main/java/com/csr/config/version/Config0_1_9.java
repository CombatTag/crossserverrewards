package com.csr.config.version;

import com.csr.config.ConfigPaths;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by derek on 11/8/2015.
 */
public abstract class Config0_1_9 {

    public static FileConfiguration update(FileConfiguration fileConfiguration) {
        fileConfiguration.set(ConfigPaths.SERVER + "." + ConfigPaths.DISPLAY, true);
        fileConfiguration.set(ConfigPaths.VERSION, "0.1.9");

        return fileConfiguration;
    }

    public static boolean equals(FileConfiguration fileConfiguration) {
        if (fileConfiguration.contains(ConfigPaths.VERSION)) {
            if (fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1.9")) {
                return true;
            }
        }
        return false;
    }
}