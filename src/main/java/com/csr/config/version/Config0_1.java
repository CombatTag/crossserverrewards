package com.csr.config.version;

import com.csr.config.ConfigPaths;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by derek on 8/28/2015.
 */
public abstract class Config0_1 {

    public static boolean equals(FileConfiguration fileConfiguration) {
        if (fileConfiguration.contains(ConfigPaths.VERSION)) {
            if (fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1") ||
                    fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1.1") ||
                    fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1.2") ||
                    fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1.3") ||
                    fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1.4") ||
                    fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1.5")) {
                return true;
            }
        }
        return false;
    }
}
