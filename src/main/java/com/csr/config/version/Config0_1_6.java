package com.csr.config.version;

import com.csr.config.ConfigPaths;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by derek on 10/11/2015.
 */
public abstract class Config0_1_6 {

    public static FileConfiguration update(FileConfiguration fileConfiguration) {
        ConfigurationSection packagesSection = fileConfiguration.getConfigurationSection(ConfigPaths.PACKAGES);

        for (String packageString : packagesSection.getKeys(false)) {
            ConfigurationSection packageSection = packagesSection.getConfigurationSection(packageString);
            ConfigurationSection rewardsSection = packageSection.getConfigurationSection(ConfigPaths.REWARDS);

            for (String reward : rewardsSection.getKeys(false)) {
                ConfigurationSection rewardSection = rewardsSection.getConfigurationSection(reward);

                rewardSection.set(ConfigPaths.CHANCE, 100);
            }
        }
        fileConfiguration.set(ConfigPaths.VERSION, "0.1.6");

        return fileConfiguration;
    }

    public static boolean equals(FileConfiguration fileConfiguration) {
        if (fileConfiguration.contains(ConfigPaths.VERSION)) {
            if (fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1.6") ||
                    fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1.7") ||
                    fileConfiguration.getString(ConfigPaths.VERSION).equals("0.1.8")) {
                return true;
            }
        }
        return false;
    }
}
