package com.csr.config;

import com.csr.config.version.Config0_1;
import com.csr.config.version.Config0_1_6;
import com.csr.config.version.Config0_1_9;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by derek on 8/28/2015.
 */
public class ConfigUpdater {

    private final FileConfiguration fileConfiguration;

    public ConfigUpdater(FileConfiguration fileConfiguration) {
        this.fileConfiguration = fileConfiguration;
    }

    public FileConfiguration update() {
        if (Config0_1.equals(fileConfiguration)) {
            Config0_1_6.update(fileConfiguration);
        }
        if (Config0_1_6.equals(fileConfiguration)) {
            Config0_1_9.update(fileConfiguration);
        }
        return fileConfiguration;
    }
}
