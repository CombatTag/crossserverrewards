package com.csr.config;

import com.csr.CrossServerRewards;
import com.csr.config.objects.*;
import com.csr.config.objects.Package;
import com.csr.util.Base64Util;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Created by derek on 8/28/2015.
 */
public class ConfigOptions {

    private FileConfiguration fileConfiguration;
    private FileConfiguration messageConfiguration;


    private String mySQLUrl;
    private String mySQLUser;
    private String mySQLPassword;

    private final Map<String, Package> packages = new HashMap<>();

    private final Map<String, Shop> shops = new HashMap<>();

    private boolean displayServer;
    private String serverName;
    private ItemStack serverItemStack;
    private String serverShop;
    private int serverSlot;
    private boolean serverRewardClaim;

    private double version;

    public ConfigOptions(FileConfiguration fileConfiguration) {
        this.fileConfiguration = fileConfiguration;

        loadConfig();
    }

    private void loadConfig() {
        this.messageConfiguration = getMessageConfiguration();

        fileConfiguration = new ConfigUpdater(fileConfiguration).update();
        fileConfiguration.options().copyDefaults(true);
        CrossServerRewards.getInstance().saveConfig();

        loadMySQL();
        loadPackages();
        loadShops();
        loadServer();

        version = fileConfiguration.getDouble(ConfigPaths.VERSION);
    }

    private void loadMySQL() {
        mySQLUrl = fileConfiguration.getString(ConfigPaths.URL);
        mySQLUser = fileConfiguration.getString(ConfigPaths.USER);
        mySQLPassword = fileConfiguration.getString(ConfigPaths.PASSWORD);
    }

    private void loadPackages() {
        ConfigurationSection packagesSection = this.fileConfiguration.getConfigurationSection(ConfigPaths.PACKAGES);

        for (String string : packagesSection.getKeys(false)) {
            ConfigurationSection packageSection = packagesSection.getConfigurationSection(string);

            try {
                List<PackageItemStack> items = new ArrayList<>();
                List<String> rewardList = packageSection.getStringList(ConfigPaths.REWARDS);

                if (!rewardList.isEmpty()) {
                    packageSection.set(ConfigPaths.REWARDS, null);
                    ConfigurationSection rewardSection = packageSection.createSection(ConfigPaths.REWARDS);
                    for (int i = 0; i < rewardList.size(); i++) {
                        ConfigurationSection itemStackSection = rewardSection.createSection(i + "");
                        String reward = rewardList.get(i);
                        ItemStack itemStack = (ItemStack) Base64Util.objectArrayFromBase64(reward)[0];

                        items.add(new PackageItemStack(100, 1, 1, itemStack));
                        saveItemStack(itemStackSection, itemStack);
                    }
                } else {
                    ConfigurationSection rewardsSection = packageSection.getConfigurationSection(ConfigPaths.REWARDS);
                    for (String reward : rewardsSection.getKeys(false)) {
                        ConfigurationSection rewardSection = rewardsSection.getConfigurationSection(reward);

                        if (rewardSection.getConfigurationSection(ConfigPaths.ITEM_STACK).getInt(ConfigPaths.AMOUNT) == 0) {
                            String[] split = rewardSection.getConfigurationSection(ConfigPaths.ITEM_STACK).getString(ConfigPaths.AMOUNT).split("-");
                            items.add(new PackageItemStack(rewardSection.getInt(ConfigPaths.CHANCE), Integer.valueOf(split[0]), Integer.valueOf(split[1]) + 1, loadItem(rewardSection)));
                        } else {
                            items.add(new PackageItemStack(rewardSection.getInt(ConfigPaths.CHANCE), 1, 1, loadItem(rewardSection)));
                        }
                    }
                }
                packages.put(string,
                        new Package(
                                loadItem(packageSection),
                                packageSection.getInt(ConfigPaths.PRICE),
                                items));
            } catch (IOException e) {
                System.out.println("An IO exception has occurred while loading packages");
                e.printStackTrace();
            }
        }
    }

    private void loadShops() {
        ConfigurationSection configurationSection = this.fileConfiguration.getConfigurationSection(ConfigPaths.SHOPS);

        for (String string : configurationSection.getKeys(false)) {
            ConfigurationSection shopSection = configurationSection.getConfigurationSection(string);

            Map<String, Package> packages = new HashMap<>();

            for (String currentPackage : shopSection.getStringList(ConfigPaths.PACKAGES)) {
                packages.put(currentPackage, this.packages.get(currentPackage));
            }

            shops.put(string,
                    new Shop(
                            shopSection.getString(ConfigPaths.INVENTORY_NAME),
                            packages));
        }
    }

    private void loadServer() {
        ConfigurationSection configurationSection = this.fileConfiguration.getConfigurationSection(ConfigPaths.SERVER);

        displayServer = configurationSection.getBoolean(ConfigPaths.DISPLAY);
        serverName = configurationSection.getString(ConfigPaths.NAME);
        serverItemStack = loadItem(configurationSection);
        serverShop = configurationSection.getString(ConfigPaths.SHOP);
        serverSlot = configurationSection.getInt(ConfigPaths.SLOT);
        serverRewardClaim = configurationSection.getBoolean(ConfigPaths.REWARD_CLAIM);
    }

    private ItemStack loadItem(ConfigurationSection configurationSection) {
        ItemStack itemStack = null;
        String itemStackString = configurationSection.getString(ConfigPaths.ITEM_STACK);
        ConfigurationSection itemConfigurationSection = configurationSection.getConfigurationSection(ConfigPaths.ITEM_STACK);

        if (itemStackString.startsWith("MemorySection")) {
            int amount = itemConfigurationSection.getInt(ConfigPaths.AMOUNT);
            itemConfigurationSection.getInt(ConfigPaths.AMOUNT);
            String displayName = itemConfigurationSection.getString(ConfigPaths.DISPLAY_NAME).replace("&", ChatColor.COLOR_CHAR + "");
            List<String> lore = itemConfigurationSection.getStringList(ConfigPaths.LORE);
            List<String> formattedLore = new ArrayList<>();
            Material material = Material.valueOf(itemConfigurationSection.getString(ConfigPaths.MATERIAL));
            byte metaData = Byte.valueOf(itemConfigurationSection.getString(ConfigPaths.META_DATA));

            itemStack = new ItemStack(material, amount, metaData);
            ItemMeta itemMeta = itemStack.getItemMeta();

            if (!displayName.equals("")) {
                itemMeta.setDisplayName(displayName);
            }
            if (!lore.isEmpty()) {
                for (String currentLore : lore) {
                    formattedLore.add(currentLore.replaceAll("&", "\247"));
                }
                itemMeta.setLore(formattedLore);
            }

            itemStack.setItemMeta(itemMeta);
        } else {
            try {
                itemStack = (ItemStack) Base64Util.objectArrayFromBase64(itemStackString)[0];
                saveItemStack(configurationSection, itemStack);
            } catch (IOException e) {
                System.out.println("An IO exception has occurred while loading a base 64 item");
                e.printStackTrace();
            }
        }
        return itemStack;
    }

    public void saveItemStack(ConfigurationSection configurationSection, ItemStack itemStack) {
        ConfigurationSection itemConfigurationSection = configurationSection.getConfigurationSection(ConfigPaths.ITEM_STACK);
        ItemMeta itemMeta = itemStack.getItemMeta();

        if (itemConfigurationSection == null) {
            itemConfigurationSection = configurationSection.createSection(ConfigPaths.ITEM_STACK);
        }

        int amount = itemStack.getAmount();
        String displayName = "";
        List<String> lore;
        List<String> formattedLore = new ArrayList<>();
        String material = itemStack.getType().name();
        String metaData = String.valueOf(itemStack.getDurability());

        if (itemStack.hasItemMeta()) {
            if (itemMeta.hasDisplayName()) {
                displayName = itemMeta.getDisplayName().replace(ChatColor.COLOR_CHAR + "", "&");
            }
            if (itemMeta.hasLore()) {
                lore = itemMeta.getLore();
                for (String currentLore : lore) {
                    formattedLore.add(currentLore.replaceAll("\247", "&"));
                }
            }
        }

        itemConfigurationSection.set(ConfigPaths.AMOUNT, amount);
        itemConfigurationSection.set(ConfigPaths.DISPLAY_NAME, displayName);
        itemConfigurationSection.set(ConfigPaths.LORE, formattedLore);
        itemConfigurationSection.set(ConfigPaths.MATERIAL, material);
        itemConfigurationSection.set(ConfigPaths.META_DATA, metaData);

        CrossServerRewards.getInstance().saveConfig();
    }

    public void reloadConfig() {
        this.messageConfiguration = null;
        loadConfig();
    }

    public FileConfiguration getMessageConfiguration() {
        if (messageConfiguration == null) {
            try {
                Reader messagesStream = new InputStreamReader(CrossServerRewards.getInstance().getResource("messages.yml"), "UTF8");
                messageConfiguration = YamlConfiguration.loadConfiguration(messagesStream);
            } catch (UnsupportedEncodingException e) {
                System.out.println("An error has occurred while loading the plugin messages.");
            }
        }

        return messageConfiguration;
    }

    public void saveConfig() {
        CrossServerRewards.getInstance().saveConfig();
    }

    public String getMySQLUrl() {
        return mySQLUrl;
    }

    public String getMySQLUser() {
        return mySQLUser;
    }

    public String getMySQLPassword() {
        return mySQLPassword;
    }

    public Map<String, Package> getPackages() {
        return packages;
    }

    public Map<String, Shop> getShops() {
        return shops;
    }

    public int getServerSlot() {
        return serverSlot;
    }

    public String getServerName() {
        return serverName;
    }

    public ItemStack getServerItemStack() {
        return serverItemStack;
    }

    public String getServerShop() {
        return serverShop;
    }

    public boolean isDisplayServer() {
        return displayServer;
    }

    public boolean isServerRewardClaim() {
        return serverRewardClaim;
    }

    public double getVersion() {
        return version;
    }
}

