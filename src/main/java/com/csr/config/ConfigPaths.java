package com.csr.config;

/**
 * Created by derek on 8/28/2015.
 */
public class ConfigPaths {

    public static final String URL = "mysql.url";
    public static final String USER = "mysql.user";
    public static final String PASSWORD = "mysql.password";

    public static final String PACKAGES = "packages";
    public static final String CHANCE = "chance";
    public static final String PRICE = "price";
    public static final String REWARDS = "rewards";

    public static final String ITEM_STACK = "itemStack";
    public static final String AMOUNT = "amount";
    public static final String DISPLAY_NAME = "displayName";
    public static final String LORE = "lore";
    public static final String MATERIAL = "material";
    public static final String META_DATA = "metaData";

    public static final String SHOPS = "shops";
    public static final String INVENTORY_NAME = "inventoryName";

    public static final String SERVER = "server";
    public static final String DISPLAY = "display";
    public static final String NAME = "name";
    public static final String SHOP = "shop";
    public static final String SLOT = "slot";
    public static final String REWARD_CLAIM = "rewardClaim";

    public static final String VERSION = "version";
}
