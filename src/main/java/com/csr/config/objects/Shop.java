package com.csr.config.objects;

import java.util.Map;

/**
 * Created by derek on 10/4/2015.
 */
public class Shop {

    private final String inventoryName;
    private final Map<String, Package> packages;

    public Shop(String inventoryName, Map<String, Package> packages) {
        this.inventoryName = inventoryName;
        this.packages = packages;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public Map<String, Package> getPackages() {
        return packages;
    }
}
