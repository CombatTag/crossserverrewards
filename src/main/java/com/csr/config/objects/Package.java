package com.csr.config.objects;

import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * Created by derek on 10/4/2015.
 */
public class Package {

    private final ItemStack displayItem;
    private final int price;
    private final List<PackageItemStack> reward;

    public Package(ItemStack displayItem, int price, List<PackageItemStack> reward) {
        this.displayItem = displayItem;
        this.price = price;
        this.reward = reward;
    }

    public ItemStack getDisplayItem() {
        return displayItem;
    }

    public int getPrice() {
        return price;
    }

    public List<ItemStack> getReward() {
        List<ItemStack> itemStackReward = new ArrayList<>();

        for (PackageItemStack packageItemStack : reward) {
            ItemStack itemStack = packageItemStack.getItemStack();

            if (itemStack != null) {
                itemStackReward.add(itemStack);
            }
        }
        return itemStackReward;
    }

    public List<PackageItemStack> getPackageItems() {
        return reward;
    }
}