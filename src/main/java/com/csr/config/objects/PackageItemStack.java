package com.csr.config.objects;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by derek on 10/4/2015.
 */
public class PackageItemStack implements ConfigurationSerializable{

    private final int chance;
    private final int minRange;
    private final int maxRange;
    private final ItemStack itemStack;

    public PackageItemStack(int chance, int minRange, int maxRange, ItemStack itemStack) {
        this.chance = chance;
        this.minRange = minRange;
        this.maxRange = maxRange;
        this.itemStack = itemStack;
    }

    public int getChance() {
        return chance;
    }

    public ItemStack getItemStack() {
        ItemStack rangeItemStack = itemStack.clone();

        if (rangeItemStack == null) {
            return null;
        }

        int random = (int)(Math.random() * (maxRange - minRange) + minRange);

        rangeItemStack.setAmount(random);

        return rangeItemStack;
    }

    public Map<String, Object> serialize() {
        LinkedHashMap result = new LinkedHashMap();
        result.put("chance", this.getChance());
        result.put("minRange", this.minRange);
        result.put("maxRange", this.maxRange);
        for (Map.Entry<String, Object> entry : itemStack.serialize().entrySet()) {
            result.put("itemStack." + entry.getKey(), entry.getValue());
        }
        return result;
    }

    public static PackageItemStack deserialize(Map<String, Object> args) {
        int chance = (int) args.get("chance");
        int minRange = (int) args.get("minRange");
        int maxRange = (int) args.get("maxRange");

        Map<String, Object> itemStackMap = new LinkedHashMap<>();

        for (Map.Entry<String, Object> entry : args.entrySet()) {
            if (entry.getKey().startsWith("itemStack.")) {
                itemStackMap.put(entry.getKey().replace("itemStack.", ""), entry.getValue());
            }
        }
        ItemStack itemStack = ItemStack.deserialize(itemStackMap);

        return new PackageItemStack(chance, minRange, maxRange, itemStack);
    }
}
