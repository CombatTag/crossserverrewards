package com.csr.database;

import com.csr.gui.Gui;
import org.bukkit.inventory.ItemStack;

import java.util.Map;
import java.util.UUID;

/**
 * Created by derek on 9/6/2015.
 */
public interface IDatabase {

    void openConnection();

    void closeConnection();

    void setupDatabase();

    void updateLocalConfig();

    void saveRewards(UUID playerUUID, String serverName, ItemStack[] itemStacks);

    void getMainGui(Callback<Gui> callback);

    void getRewardGui(UUID playerUUID, String serverName, Callback<Gui> callback);

    void getServerGuis(Callback<Map<String, Gui>> callback);
}
