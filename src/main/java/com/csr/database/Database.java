package com.csr.database;

import com.csr.CrossServerRewards;
import com.csr.config.ConfigOptions;
import com.csr.config.objects.*;
import com.csr.config.objects.Package;
import com.csr.gui.Gui;
import com.csr.gui.RewardGui;
import com.csr.gui.items.GuiItem;
import com.csr.gui.items.PackageItem;
import com.csr.gui.items.ServerItem;
import com.csr.util.ChatUtil;
import com.csr.util.Base64Util;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by derek on 9/6/2015.
 */
public class Database implements IDatabase {

    private final String rewardTable;
    private final String packageTable;
    private final String serverTable;
    private final String shopTable;

    private Connection connection;
    private ExecutorService executorService = null;

    private final String url;
    private final String user;
    private final String password;

    public Database(String url, String user, String password) {
        this.rewardTable = "rewards";
        this.packageTable = "packages";
        this.serverTable = "servers";
        this.shopTable = "shops";

        this.url = url;
        this.user = user;
        this.password = password;

        this.executorService = Executors.newFixedThreadPool(1);
    }

    @Override
    public void openConnection() {
        try {
            if (connection == null) {
                connection = DriverManager.getConnection(url, user, password);
            }
        } catch (SQLException e) {
            System.out.println("An SQL exception has occurred while opening a connection");
            e.printStackTrace();
        }
    }

    @Override
    public void closeConnection() {
        if (connection != null) {
            try {
                if (!connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                System.out.println("An SQL exception has occurred while closing a connection");
                e.printStackTrace();
            }
        }
    }

    private Connection getConnection()
    {
        try {
            if (connection == null || connection.isClosed()) {
                connection = DriverManager.getConnection(url, user, password);
            }
        } catch (SQLException e) {
            System.out.println("Unable to determine if the connection was closed.");
        }

        return connection;
    }

    @Override
    public void setupDatabase() {
        PreparedStatement rewards = null, servers = null, packages = null, shops = null;
        Statement createColumn = null;
        try {
            rewards = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `" + rewardTable + "` (serverName TEXT NOT NULL, playerUUID TEXT NOT NULL, itemStacks TEXT NOT NULL);");
            rewards.executeUpdate();
            servers = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `" + serverTable + "` (display TINYINT NOT NULL, serverName TEXT NOT NULL, slot REAL NOT NULL, itemStack TEXT NOT NULL, shopName TEXT NOT NULL);");
            servers.executeUpdate();
            packages = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `" + packageTable + "` (serverName TEXT NOT NULL, packageName TEXT NOT NULL, itemStack TEXT NOT NULL, price REAL NOT NULL, packageItemStacks TEXT NOT NULL);");
            packages.executeUpdate();
            shops = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `" + shopTable + "` (serverName TEXT NOT NULL, shopName TEXT NOT NULL, inventoryName TEXT NOT NULL, packageNames TEXT NOT NULL);");
            shops.executeUpdate();

            DatabaseMetaData md = getConnection().getMetaData();
            ResultSet rs = md.getColumns(null, null, serverTable, "display");
            if (!rs.next()) {
                createColumn = getConnection().createStatement();
                createColumn.executeUpdate("ALTER TABLE " + serverTable + " ADD display TINYINT FIRST");
            }
        } catch (SQLException e) {
            System.out.println("An SQL exception has occurred while setting up the database");
            e.printStackTrace();
        } finally {
            try {
                if (rewards != null) {
                    rewards.close();
                }
                if (servers != null) {
                    servers.close();
                }
                if (packages != null) {
                    packages.close();
                }
                if (shops != null) {
                    shops.close();
                }
                if (createColumn != null) {
                    createColumn.close();
                }
            } catch (SQLException e) {
                System.out.println("An SQL exception has occurred while closing the prepared statement");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateLocalConfig() {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                updatePackages();
                updateShops();
                updateServer();
            }
        });
    }

    private void updatePackages() {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = getConnection().prepareStatement("DELETE FROM " + packageTable + " WHERE serverName=?");

            preparedStatement.setString(1, CrossServerRewards.getInstance().getConfigOptions().getServerName());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.out.println("An SQL exception has occurred while updating the packages");
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }

                for (Map.Entry<String, Package> entry : CrossServerRewards.getInstance().getConfigOptions().getPackages().entrySet()) {
                    PreparedStatement preparedStatement1 = null;

                    try {
                        ConfigOptions configOptions = CrossServerRewards.getInstance().getConfigOptions();

                        preparedStatement1 = getConnection().prepareStatement("INSERT INTO " + packageTable + " (serverName, packageName, itemStack, price, packageItemStacks) VALUES (?, ?, ?, ?, ?)");

                        preparedStatement1.setString(1, configOptions.getServerName());
                        preparedStatement1.setString(2, entry.getKey());
                        preparedStatement1.setString(3, Base64Util.objectArrayToBase64(Collections.singletonList(entry.getValue().getDisplayItem()).toArray(new ItemStack[1])));
                        preparedStatement1.setInt(4, entry.getValue().getPrice());
                        preparedStatement1.setString(5, Base64Util.objectArrayToBase64(entry.getValue().getPackageItems().toArray(new PackageItemStack[entry.getValue().getPackageItems().size()])));

                        preparedStatement1.executeUpdate();
                    } catch (SQLException e) {
                        System.out.println("An SQL exception has occurred while updating the packages");
                        e.printStackTrace();
                    } finally {
                        try {
                            if (preparedStatement1 != null) {
                                preparedStatement1.close();
                            }
                        } catch (SQLException e) {
                            System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                            e.printStackTrace();
                        }
                    }
                }
            } catch (SQLException e) {
                System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                e.printStackTrace();
            }
        }
    }

    private void updateShops() {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = getConnection().prepareStatement("DELETE FROM " + shopTable + " WHERE serverName=?");

            preparedStatement.setString(1, CrossServerRewards.getInstance().getConfigOptions().getServerName());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.out.println("An SQL exception has occurred while updating the shops");
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }

                for (Map.Entry<String, Shop> entry : CrossServerRewards.getInstance().getConfigOptions().getShops().entrySet()) {
                    PreparedStatement preparedStatement1 = null;

                    try {
                        ConfigOptions configOptions = CrossServerRewards.getInstance().getConfigOptions();

                        preparedStatement1 = getConnection().prepareStatement("INSERT INTO " + shopTable + " (serverName, shopName, inventoryName, packageNames) VALUES (?, ?, ?, ?)");

                        StringBuilder packageNames = new StringBuilder();

                        Iterator<String> iterator = entry.getValue().getPackages().keySet().iterator();
                        while (iterator.hasNext()) {
                            packageNames.append(iterator.next());
                            if (iterator.hasNext()) {
                                packageNames.append(",");
                            }
                        }

                        preparedStatement1.setString(1, configOptions.getServerName());
                        preparedStatement1.setString(2, entry.getKey());
                        preparedStatement1.setString(3, entry.getValue().getInventoryName());
                        preparedStatement1.setString(4, packageNames.toString());

                        preparedStatement1.executeUpdate();
                    } catch (SQLException e) {
                        System.out.println("An SQL exception has occurred while updating the shops");
                        e.printStackTrace();
                    } finally {
                        try {
                            if (preparedStatement1 != null) {
                                preparedStatement1.close();
                            }
                        } catch (SQLException e) {
                            System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                            e.printStackTrace();
                        }
                    }
                }
            } catch (SQLException e) {
                System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                e.printStackTrace();
            }
        }
    }

    private void updateServer() {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = getConnection().prepareStatement("DELETE FROM " + serverTable + " WHERE serverName=?");

            preparedStatement.setString(1, CrossServerRewards.getInstance().getConfigOptions().getServerName());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.out.println("An SQL exception has occurred while updating the server");
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }

                PreparedStatement preparedStatement1 = null;

                try {
                    ConfigOptions configOptions = CrossServerRewards.getInstance().getConfigOptions();

                    preparedStatement1 = getConnection().prepareStatement("INSERT INTO " + serverTable + " (display, serverName, slot, itemStack, shopName) VALUES (?, ?, ?, ?, ?)");

                    preparedStatement1.setInt(1, configOptions.isDisplayServer() ? 1 : 0);
                    preparedStatement1.setString(2, configOptions.getServerName());
                    preparedStatement1.setInt(3, configOptions.getServerSlot());
                    preparedStatement1.setString(4, Base64Util.objectArrayToBase64(new ItemStack[]{configOptions.getServerItemStack()}));
                    preparedStatement1.setString(5, configOptions.getServerShop());

                    preparedStatement1.executeUpdate();
                } catch (SQLException e) {
                    System.out.println("An SQL exception has occurred while updating the shops");
                    e.printStackTrace();
                } finally {
                    try {
                        if (preparedStatement1 != null) {
                            preparedStatement1.close();
                        }
                    } catch (SQLException e) {
                        System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                        e.printStackTrace();
                    }
                }
            } catch (SQLException e) {
                System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void saveRewards(final UUID playerUUID, final String serverName, final ItemStack[] itemStacks) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                Statement statement = null;

                try {
                    statement = getConnection().createStatement();

                    statement.executeUpdate("DELETE FROM " + rewardTable + " WHERE serverName='" + serverName + "' AND playerUUID='" + playerUUID.toString() + "'");
                    statement.executeUpdate("INSERT INTO " + rewardTable + " VALUES ('" + serverName + "', '" + playerUUID.toString() + "', '" + Base64Util.objectArrayToBase64(itemStacks) + "')");
                } catch (SQLException e) {
                    System.out.println("An SQL exception has occurred while updating the server");
                    e.printStackTrace();
                } finally {
                    try {
                        if (statement != null) {
                            statement.close();
                        }
                    } catch (SQLException e) {
                        System.out.println("An SQL exception has occurred while closing the statement");
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void getRewardGui(final UUID playerUUID, final String serverName, final Callback<Gui> callback) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                getRewards(playerUUID, serverName, new Callback<List<ItemStack>>() {
                    @Override
                    public void onSuccess(List<ItemStack> value) {
                        Map<Integer, GuiItem> items = new HashMap<>();
                        for (int i = 0; value.size() > i; i++) {
                            items.put(i, new GuiItem(value.get(i)));
                        }
                        callback.onSuccess(new RewardGui(null, ChatUtil.getPlayerName(playerUUID) + "'s Rewards", items, serverName));
                    }

                    @Override
                    public void onFailure() {
                        callback.onSuccess(new RewardGui(null, ChatUtil.getPlayerName(playerUUID) + "'s Rewards", new HashMap<Integer, GuiItem>(), serverName));
                    }
                });
            }
        });
    }

    public void getRewards(final UUID playerUUID, final String serverName, final Callback<List<ItemStack>> callback) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;

                try {
                    preparedStatement = getConnection().prepareStatement("SELECT * FROM " + rewardTable + " WHERE serverName=? AND playerUUID=?");

                    preparedStatement.setString(1, serverName);
                    preparedStatement.setString(2, playerUUID.toString());

                    resultSet = preparedStatement.executeQuery();
                    try {
                        if (resultSet.next()) {
                            List<ItemStack> itemStacks = new ArrayList<>();

                            for (Object object : Arrays.asList(Base64Util.objectArrayFromBase64(resultSet.getString("itemStacks")))) {
                                if (object instanceof ItemStack) {
                                    itemStacks.add((ItemStack) object);
                                }
                            }
                            callback.onSuccess(itemStacks);
                        } else {
                            callback.onFailure();
                        }
                    } catch (IOException e) {
                        System.out.println("An IO exception has occurred while converting " + ChatUtil.getPlayerName(playerUUID) + "'s rewards from base 64 to itemstacks");
                        e.printStackTrace();
                    }
                } catch (SQLException e) {
                    System.out.println("An SQL exception has occurred while loading the rewards");
                    e.printStackTrace();
                } finally {
                    try {
                        if (preparedStatement != null) {
                            preparedStatement.close();
                        }

                        if (resultSet != null) {
                            resultSet.close();
                        }
                    } catch (SQLException e) {
                        System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    @Override
    public void getMainGui(final Callback<Gui> callback) {
        executorService.submit(new Runnable() {

            final List<String> serverNames = new ArrayList<>();

            @Override
            public void run() {
                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;

                try {
                    preparedStatement = getConnection().prepareStatement("SELECT serverName FROM " + serverTable);
                    resultSet = preparedStatement.executeQuery();

                    while (resultSet.next()) {
                        serverNames.add(resultSet.getString("serverName"));
                    }
                } catch (SQLException e) {
                    System.out.println("An SQL exception has occurred while loading the servers");
                    e.printStackTrace();
                } finally {
                    try {
                        if (preparedStatement != null) {
                            preparedStatement.close();
                        }

                        if (resultSet != null) {
                            resultSet.close();
                        }
                    } catch (SQLException e) {
                        System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                        e.printStackTrace();
                    }

                    Map<Integer, GuiItem> items = new HashMap<>();

                    for (final String serverName : serverNames) {
                        PreparedStatement preparedStatement1 = null;
                        ResultSet resultSet1 = null;

                        try {
                            preparedStatement1 = getConnection().prepareStatement("SELECT * FROM " + serverTable + " WHERE serverName=?");
                            preparedStatement1.setString(1, serverName);
                            resultSet1 = preparedStatement1.executeQuery();

                            if (resultSet1.next()) {
                                try {
                                    if (resultSet1.getBoolean("display")) {
                                        items.put(resultSet1.getInt("slot"), new ServerItem((ItemStack) (Base64Util.objectArrayFromBase64(resultSet1.getString("itemStack"))[0]), serverName));
                                    }
                                } catch (IOException e) {
                                    System.out.println("An IO exception has occurred while converting the server " + serverName + " from base 64 to an itemstack");
                                    e.printStackTrace();
                                }
                            }
                        } catch (SQLException e) {
                            System.out.println("An SQL exception has occurred while loading servers");
                            e.printStackTrace();
                        } finally {
                            try {
                                if (preparedStatement1 != null) {
                                    preparedStatement1.close();
                                }

                                if (resultSet1 != null) {
                                    resultSet1.close();
                                }
                            } catch (SQLException e) {
                                System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                                e.printStackTrace();
                            }
                        }
                    }
                    callback.onSuccess(new Gui(null, "Rewards", items));
                }
            }
        });
    }

    @Override
    public void getServerGuis(final Callback<Map<String, Gui>> callback) {
        executorService.submit(new Runnable() {

            final List<String> serverNames = new ArrayList<>();

            @Override
            public void run() {
                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;

                try {
                    preparedStatement = getConnection().prepareStatement("SELECT serverName FROM " + serverTable);
                    resultSet = preparedStatement.executeQuery();

                    while (resultSet.next()) {
                        serverNames.add(resultSet.getString("serverName"));
                    }
                } catch (SQLException e) {
                    System.out.println("An SQL exception has occurred while loading the server GUIs");
                    e.printStackTrace();
                } finally {
                    try {
                        if (preparedStatement != null) {
                            preparedStatement.close();
                        }

                        if (resultSet != null) {
                            resultSet.close();
                        }
                    } catch (SQLException e) {
                        System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                        e.printStackTrace();
                    }

                    Map<String, String> shops = new HashMap<>();

                    for (final String serverName : serverNames) {
                        PreparedStatement preparedStatement1 = null;
                        ResultSet resultSet1 = null;

                        try {
                            preparedStatement1 = getConnection().prepareStatement("SELECT * FROM " + serverTable + " WHERE serverName=?");
                            preparedStatement1.setString(1, serverName);
                            resultSet1 = preparedStatement1.executeQuery();

                            if (resultSet1.next()) {
                                shops.put(serverName, resultSet1.getString("shopName"));
                            }
                        } catch (SQLException e) {
                            System.out.println("An SQL exception has occurred while loading server GUIs");
                            e.printStackTrace();
                        } finally {
                            try {
                                if (preparedStatement1 != null) {
                                    preparedStatement1.close();
                                }

                                if (resultSet1 != null) {
                                    resultSet1.close();
                                }
                            } catch (SQLException e) {
                                System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                                e.printStackTrace();
                            }
                        }
                    }

                    getShops(shops, new Callback<Map<String, Gui>>() {
                        @Override
                        public void onSuccess(Map<String, Gui> value) {
                            callback.onSuccess(value);
                        }

                        @Override
                        public void onFailure() {}
                    });
                }
            }
        });
    }

    private void getShops(Map<String, String> serverShops, Callback<Map<String, Gui>> callback) {
        final Map<String, Gui> shops = new HashMap<>();

        for (final Map.Entry<String, String> entry : serverShops.entrySet()) {
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;

            try {
                preparedStatement = getConnection().prepareStatement("SELECT * FROM " + shopTable + " WHERE shopName=?");
                preparedStatement.setString(1, entry.getValue());
                resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    final String inventoryName = resultSet.getString("inventoryName");

                    getPackageGui(entry.getKey(), resultSet.getString("packageNames").split(","), new Callback<Map<Integer, GuiItem>>() {
                        @Override
                        public void onSuccess(Map<Integer, GuiItem> value) {
                            shops.put(entry.getKey(), new Gui(CrossServerRewards.getInstance().getGuiManager().getMainGui(), inventoryName, value));
                        }

                        @Override
                        public void onFailure() {}
                    });
                }
                callback.onSuccess(shops);
            } catch (SQLException e) {
                System.out.println("An SQL exception has occurred while loading the shops");
                e.printStackTrace();
            } finally {
                try {
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }

                    if (resultSet != null) {
                        resultSet.close();
                    }
                } catch (SQLException e) {
                    System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                    e.printStackTrace();
                }
            }
        }
    }

    private void getPackageGui(String serverName, String[] packageNames, Callback<Map<Integer, GuiItem>> callback) {
        Map<Integer, GuiItem> items = new HashMap<>();

        for (int i = 0; packageNames.length > i; i++) {
            String packageName = packageNames[i];
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;

            try {
                preparedStatement = getConnection().prepareStatement("SELECT * FROM " + packageTable + " WHERE packageName=?");
                preparedStatement.setString(1, packageName);
                resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    try {
                        List<PackageItemStack> packageItemStacks = new ArrayList<>();

                        for (Object object : Arrays.asList(Base64Util.objectArrayFromBase64(resultSet.getString("packageItemStacks")))) {
                            if (object instanceof PackageItemStack) {
                                packageItemStacks.add((PackageItemStack) object);
                            }
                        }

                        items.put(i, new PackageItem((ItemStack) Base64Util.objectArrayFromBase64(resultSet.getString("itemStack"))[0], serverName, resultSet.getInt("price"), packageItemStacks.toArray(new PackageItemStack[packageItemStacks.size()])));
                    } catch (IOException e) {
                        System.out.println("An IO exception has occurred while converting the package " + packageName + " from base 64 to an itemstack");
                        e.printStackTrace();
                    }
                }
                callback.onSuccess(items);
            } catch (SQLException e) {
                System.out.println("An SQL exception has occurred while loading the package GUIs");
                e.printStackTrace();
            } finally {
                try {
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }

                    if (resultSet != null) {
                        resultSet.close();
                    }
                } catch (SQLException e) {
                    System.out.println("An SQL exception has occurred while closing the prepared statement and result set");
                    e.printStackTrace();
                }
            }
        }
    }
}
