package com.csr.database;

/**
 * Created by derek on 9/7/2015.
 */
public interface Callback<V> {

    void onSuccess(V value);

    void onFailure();
}