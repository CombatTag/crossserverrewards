package com.csr.util;

import com.csr.CrossServerRewards;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Method;
import java.util.UUID;

/**
 * Created by derek on 8/28/2015.
 */
public class ChatUtil {

    public static String t(final String string, final String... values) {
        String message = CrossServerRewards.getInstance().getConfigOptions().getMessageConfiguration().getString(string);
        int index = 0;
        while (message.contains("{" + index + "}")) {
            message = message.replace("{" + index + "}", values[index]).replace("?", "\247");
            index++;
        }
        return message;
    }

    public static String getPlayerName(UUID playerUUID) {
        Player player = Bukkit.getPlayer(playerUUID);

        if (player == null) {
            return "<NULL>";
        } else {
            return player.getName();
        }
    }

    public static String convertItemStackToJson(ItemStack itemStack) {
        Class<?> craftItemStackClazz = ReflectionUtil.getOBCClass("inventory.CraftItemStack");
        Method asNMSCopyMethod = ReflectionUtil.getMethod(craftItemStackClazz, "asNMSCopy", ItemStack.class);

        Class<?> nmsItemStackClazz = ReflectionUtil.getNMSClass("ItemStack");
        Class<?> nbtTagCompoundClazz = ReflectionUtil.getNMSClass("NBTTagCompound");
        Method saveNmsItemStackMethod = ReflectionUtil.getMethod(nmsItemStackClazz, "save", nbtTagCompoundClazz);

        Object nmsNbtTagCompoundObj;
        Object nmsItemStackObj;
        Object itemAsJsonObject;

        try {
            nmsNbtTagCompoundObj = nbtTagCompoundClazz.newInstance();
            nmsItemStackObj = asNMSCopyMethod.invoke(null, itemStack);
            itemAsJsonObject = saveNmsItemStackMethod.invoke(nmsItemStackObj, nmsNbtTagCompoundObj);
        } catch (Throwable t) {
            System.out.println("Failed to serialize itemstack to nsm item");
            t.printStackTrace();
            return null;
        }

        return itemAsJsonObject.toString();
    }
}
