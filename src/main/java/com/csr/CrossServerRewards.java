package com.csr;

import com.csr.command.CommandManager;
import com.csr.config.ConfigOptions;
import com.csr.database.Database;
import com.csr.database.IDatabase;
import com.csr.gui.GuiManager;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by derek on 8/28/2015.
 */
public class CrossServerRewards extends JavaPlugin {

    private static CrossServerRewards instance;

    private ConfigOptions configOptions;
    private GuiManager guiManager;
    private IDatabase database;

    private Economy economy;

    @Override
    public void onEnable() {
        instance = this;

        configOptions = new ConfigOptions(getConfig());
        database = new Database(configOptions.getMySQLUrl(), configOptions.getMySQLUser(), configOptions.getMySQLPassword());

        database.closeConnection();
        database.openConnection();
        database.setupDatabase();
        database.updateLocalConfig();

        guiManager = new GuiManager();
        new CommandManager();

        if (!registerEconomy()) {
            Bukkit.getPluginManager().disablePlugin(this);
            System.out.println("This plugin requires Vault, install the plugin and restart.");
        }
    }

    private boolean registerEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }

    @Override
    public void onDisable() {
        database.closeConnection();
    }

    public static CrossServerRewards getInstance() {
        return instance;
    }

    public ConfigOptions getConfigOptions() {
        return configOptions;
    }

    public GuiManager getGuiManager() {
        return guiManager;
    }

    public IDatabase getIDatabase() {
        return database;
    }

    public Economy getEconomy() {
        return economy;
    }
}
